<?php

namespace App\Context\Infrastructure\Ports\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use App\Application as UseCase;

class IndexController
{

    public function getCreateCanvas(Request $request): Response
    {
        $canvasName = strtolower($request->get('name') ?? '');
        $canvasWidth = (int) ($request->get('width') ?? 0);
        $canvasHeight = (int) ($request->get('height') ?? 0);

        try {
            $result = call_user_func_array(new UseCase\CreateCanvasUseCase(), [$canvasName, $canvasWidth, $canvasHeight]);
            return new JsonResponse([
                'status' => 'created',
                'canvas' => $result
                    ], Response::HTTP_CREATED);
        } catch (\Exception $exception) {
            return new JsonResponse(['errors' => [$exception->getMessage()]], $exception->getCode());
        }
    }

    public function getMove($canvasName, $movementDirection): Response
    {
        try {
            $result = call_user_func_array(new UseCase\MoveCanvasUseCase(), [$canvasName, $movementDirection]);
            return new JsonResponse([
                'status' => 'moved',
                'canvas' => $result
            ]);
        } catch (\Exception $exception) {
            return new JsonResponse(['errors' => [$exception->getMessage()]], $exception->getCode());
        }
    }

}
