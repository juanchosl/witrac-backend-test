<?php

namespace App\Context\Domain\Contracts;

use App\Context\Domain\Entities\Canvas;

interface CacheInterface
{

    public function __construct($cache_name);

    public function delete();

    public function set(Canvas $data): Canvas;

    public function get(): Canvas;

    public function hasItem();
}
