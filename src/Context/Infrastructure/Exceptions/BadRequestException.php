<?php

namespace App\Context\Infrastructure\Exceptions;

use Symfony\Component\HttpFoundation\Response;

class BadRequestException extends \Exception
{

    public function __construct(string $message = "")
    {
        return parent::__construct($message, Response::HTTP_BAD_REQUEST);
    }

}
