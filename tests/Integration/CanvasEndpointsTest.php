<?php

namespace App\Tests\Integration;

class CanvasEndpointsTest extends Endpoints
{

    protected function checkResponseStructure($element)
    {
        $this->assertTrue(is_object($element));
        $this->assertObjectHasAttribute('status', $element);
        $this->assertObjectHasAttribute('canvas', $element);
        $this->assertTrue(is_object($element->canvas));
        $this->assertObjectHasAttribute('name', $element->canvas);
        $this->assertEquals($this->canvas_name, $element->canvas->name);

        $this->assertObjectHasAttribute('width', $element->canvas);
        $this->assertEquals($this->canvas_width, $element->canvas->width);
        $this->assertIsInt($element->canvas->width);

        $this->assertObjectHasAttribute('height', $element->canvas);
        $this->assertEquals($this->canvas_height, $element->canvas->height);
        $this->assertIsInt($element->canvas->height);

        $this->assertObjectHasAttribute('spaceship', $element->canvas);
        $this->assertObjectHasAttribute('x', $element->canvas->spaceship);
        $this->assertIsInt($element->canvas->spaceship->x);

        $this->assertObjectHasAttribute('y', $element->canvas->spaceship);
        $this->assertIsInt($element->canvas->spaceship->y);
    }

    public function testCreateCanvas()
    {
        $element = $this->remote("{$this->url}/create-canvas", 'GET', ['name' => $this->canvas_name, 'width' => $this->canvas_width, 'height' => $this->canvas_height], 201);
        $this->checkResponseStructure($element);
        $this->assertEquals('created', $element->status);
        $this->assertEquals(0, $element->canvas->spaceship->x);
        $this->assertEquals(0, $element->canvas->spaceship->y);
    }

    public function testMoveRightCanvas()
    {
        $element = $this->remote("{$this->url}/move/{$this->canvas_name}/right", 'GET', [], 200);
        $this->checkResponseStructure($element);

        $this->assertEquals('moved', $element->status);
        $this->assertEquals(1, $element->canvas->spaceship->x);
        $this->assertEquals(0, $element->canvas->spaceship->y);
    }

    public function testMoveBottomCanvas()
    {
        $element = $this->remote("{$this->url}/move/{$this->canvas_name}/bottom", 'GET', [], 200);
        $this->checkResponseStructure($element);

        $this->assertEquals('moved', $element->status);
        $this->assertEquals(1, $element->canvas->spaceship->x);
        $this->assertEquals(1, $element->canvas->spaceship->y);
    }

    public function testMoveLeftCanvas()
    {
        $element = $this->remote("{$this->url}/move/{$this->canvas_name}/left", 'GET', [], 200);
        $this->checkResponseStructure($element);

        $this->assertEquals('moved', $element->status);
        $this->assertEquals(0, $element->canvas->spaceship->x);
        $this->assertEquals(1, $element->canvas->spaceship->y);
    }

    public function testMoveTopCanvas()
    {
        $element = $this->remote("{$this->url}/move/{$this->canvas_name}/top", 'GET', [], 200);
        $this->checkResponseStructure($element);

        $this->assertEquals('moved', $element->status);
        $this->assertEquals(0, $element->canvas->spaceship->x);
        $this->assertEquals(0, $element->canvas->spaceship->y);
    }

    public function testMoveOutboundXCanvas()
    {
        $element = $this->remote("{$this->url}/move/{$this->canvas_name}/left", 'GET', [], 200);
        $this->checkResponseStructure($element);

        $this->assertEquals('moved', $element->status);
        $this->assertEquals(4, $element->canvas->spaceship->x);
        $this->assertEquals(0, $element->canvas->spaceship->y);
    }

    public function testMoveOutboundYCanvas()
    {
        $element = $this->remote("{$this->url}/move/{$this->canvas_name}/top", 'GET', [], 200);
        $this->checkResponseStructure($element);

        $this->assertEquals('moved', $element->status);
        $this->assertEquals(4, $element->canvas->spaceship->x);
        $this->assertEquals(4, $element->canvas->spaceship->y);
    }

}
