<?php

namespace App\Tests\Integration;

use App\Tests\BaseTesting;

class Endpoints extends BaseTesting
{

    protected $url = 'http://localhost';

    protected function remote($url, $method = 'GET', $params = [], $desiredCode = 200)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $method);
        if (!empty($params)) {
            if ($method != 'POST') {
                $params = http_build_query($params);
                if ($method == 'GET') {
                    $url .= '?' . $params;
                } else {
                    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Content-Type: application/x-www-form-urlencoded']);
                }
            }
            curl_setopt($ch, CURLOPT_POSTFIELDS, $params);
        }
        curl_setopt($ch, CURLOPT_URL, $url);
        $response = curl_exec($ch);
        $httpCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
        $this->assertEquals($desiredCode, $httpCode);
        curl_close($ch);
        return json_decode($response);
    }

}
