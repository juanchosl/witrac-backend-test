<?php

namespace App\Context\Infrastructure\Adapters;

use App\Context\Domain\Contracts\CacheInterface;
use Symfony\Component\Cache\Adapter\FilesystemAdapter;
use Symfony\Contracts\Cache\ItemInterface;
use App\Context\Domain\Entities\Canvas;
use App\Context\Domain\Entities\Spaceship;

class CacheAdapter implements CacheInterface
{

    private $cache_name;
    private $cache;

    public static function getInstance($cache_name)
    {
        return new static($cache_name);
    }

    public function __construct($cache_name)
    {
        $this->cache_name = $cache_name;
        $this->cache = new FilesystemAdapter();
    }

    public function delete()
    {
        return $this->cache->delete($this->cache_name);
    }

    public function set(Canvas $data): Canvas
    {
        $this->cache->delete($this->cache_name);
        $data = json_decode(json_encode($data), true);
        $this->cache->get($this->cache_name, function (ItemInterface $item) use ($data) {
            return $data;
        });
        return $this->get();
    }

    public function get(): Canvas
    {
        $canvas = $this->cache->getItem($this->cache_name)->get();
        $spaceshipObj = new Spaceship();
        $spaceshipObj->setX($canvas['spaceship']['x']);
        $spaceshipObj->setY($canvas['spaceship']['y']);
        $canvasObj = new Canvas();
        $canvasObj->setName($canvas['name']);
        $canvasObj->setWidth($canvas['width']);
        $canvasObj->setHeight($canvas['height']);
        $canvasObj->setSpaceship($spaceshipObj);
        return $canvasObj;
    }

    public function hasItem()
    {
        return $this->cache->hasItem($this->cache_name);
    }

}
