<?php

namespace App\Context\Domain\Entities;

class Spaceship implements \JsonSerializable
{

    private $x = 0;
    private $y = 0;

    public function getX(): int
    {
        return $this->x;
    }

    public function getY(): int
    {
        return $this->y;
    }

    public function setX($x): void
    {
        $this->x = $x;
    }

    public function setY($y): void
    {
        $this->y = $y;
    }

    public function jsonSerialize(): mixed
    {
        return [
            'x' => $this->getX(),
            'y' => $this->getY()
        ];
    }

}
