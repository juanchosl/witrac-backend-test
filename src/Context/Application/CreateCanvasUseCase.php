<?php

namespace App\Application;

use App\Context\Domain\CanvasService;
use App\Context\Domain\Entities\Canvas;
use App\Context\Domain\Entities\Spaceship;
use App\Context\Infrastructure\Adapters\CacheAdapter;
use App\Context\Infrastructure\Exceptions\BadRequestException;

class CreateCanvasUseCase
{

    public function __invoke($canvas_name, int $canvas_width, int $canvas_height)
    {
        if (!$canvas_name) {
            throw new BadRequestException('Missing request parameter "name".');
        }

        if (!$canvas_width) {
            throw new BadRequestException('Missing or invalid value of request parameter "width".');
        }

        if (!$canvas_height) {
            throw new BadRequestException('Missing or invalid value of request parameter "height".');
        }

        $canvas = new Canvas();
        $canvas->setName($canvas_name);
        $canvas->setWidth($canvas_width);
        $canvas->setHeight($canvas_height);
        $canvas->setSpaceship(new Spaceship());

        return CanvasService::getInstance(CacheAdapter::getInstance('canvas_' . $canvas_name))->create($canvas);
    }

}
