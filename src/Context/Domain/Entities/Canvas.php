<?php

namespace App\Context\Domain\Entities;

class Canvas implements \JsonSerializable
{

    private $name;
    private $width;
    private $height;
    private $spaceship;

    public function getName()
    {
        return $this->name;
    }

    public function getWidth(): int
    {
        return $this->width;
    }

    public function getHeight(): int
    {
        return $this->height;
    }

    public function getSpaceship(): Spaceship
    {
        return $this->spaceship;
    }

    public function setName($name): void
    {
        $this->name = $name;
    }

    public function setWidth(int $width): void
    {
        $this->width = $width;
    }

    public function setHeight(int $height): void
    {
        $this->height = $height;
    }

    public function setSpaceship(Spaceship $spaceship): void
    {
        $this->spaceship = $spaceship;
    }

    public function jsonSerialize(): mixed
    {
        return [
            'name' => $this->getName(),
            'width' => $this->getWidth(),
            'height' => $this->getHeight(),
            'spaceship' => $this->getSpaceship(),
        ];
    }

}
