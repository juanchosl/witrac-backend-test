<?php

namespace App\Application;

use App\Context\Domain\CanvasService;
use App\Context\Infrastructure\Adapters\CacheAdapter;
use App\Context\Infrastructure\Exceptions\BadRequestException;

class MoveCanvasUseCase
{

    public function __invoke($canvas_name, $movement_direction)
    {
        if (!$canvas_name) {
            throw new BadRequestException('Missing value of endpoint parameter "canvasName".');
        }
        if (!$movement_direction) {
            throw new BadRequestException('Missing value of endpoint parameter "movementDirection".');
        }

        $cache = CacheAdapter::getInstance('canvas_' . $canvas_name);
        if (!$cache->hasItem()) {
            throw new BadRequestException('Missing canvas "' . $canvas_name . '".');
        }
        return CanvasService::getInstance($cache)->move($movement_direction);
    }

}
