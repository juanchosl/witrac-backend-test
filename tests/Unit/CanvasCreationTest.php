<?php

namespace App\Tests\Unit;

use App\Tests\BaseTesting;
use App\Context\Domain\Entities\Canvas;
use App\Context\Domain\Entities\Spaceship;
use App\Context\Infrastructure\Adapters\CacheAdapter;
use App\Context\Domain\CanvasService;

class CanvasCreationTest extends BaseTesting
{

    protected $cache;
    protected $service;

    public function setUp(): void
    {
        $this->cache = CacheAdapter::getInstance("canvas_{$this->canvas_name}");
        $this->service = CanvasService::getInstance($this->cache);
    }

    public function testCreateCanvas()
    {
        $canvas = new Canvas();
        $canvas->setName($this->canvas_name);
        $canvas->setWidth($this->canvas_width);
        $canvas->setHeight($this->canvas_height);
        $canvas->setSpaceship(new Spaceship());

        $result = $this->service->create($canvas);
        $this->assertInstanceOf(Canvas::class, $result);
        $this->assertEquals($this->canvas_name, $result->getName());
        $this->assertEquals($this->canvas_width, $result->getWidth());
        $this->assertEquals($this->canvas_height, $result->getHeight());
        $this->assertEquals(0, $result->getSpaceship()->getX());
        $this->assertEquals(0, $result->getSpaceship()->getY());
    }

}
