<?php

namespace App\Tests\Integration;

class CanvasEndpointsErrorsTest extends Endpoints
{

    protected function checkErrorStructure($element)
    {
        $this->assertTrue(is_object($element));
        $this->assertObjectHasAttribute('errors', $element);
        $this->assertIsArray($element->errors);
    }

    public function testInvalidMovement()
    {
        $element = $this->remote("{$this->url}/move/{$this->canvas_name}/tope", 'GET', [], 400);
        $this->checkErrorStructure($element);
    }

    public function testCreateCanvasWithoutName()
    {
        $element = $this->remote("{$this->url}/create-canvas", 'GET', ['width' => $this->canvas_width, 'height' => $this->canvas_height], 400);
        $this->checkErrorStructure($element);
    }

    public function testCreateCanvasWithoutWidth()
    {
        $element = $this->remote("{$this->url}/create-canvas", 'GET', ['name' => $this->canvas_name, 'height' => $this->canvas_height], 400);
        $this->checkErrorStructure($element);
    }

    public function testCreateCanvasWithoutHeight()
    {
        $element = $this->remote("{$this->url}/create-canvas", 'GET', ['width' => $this->canvas_width, 'name' => $this->canvas_name], 400);
        $this->checkErrorStructure($element);
    }

}
