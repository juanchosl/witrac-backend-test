<?php

namespace App\Context\Domain;

use App\Context\Domain\Contracts\CacheInterface;
use App\Context\Domain\Entities\Canvas;
use App\Context\Infrastructure\Exceptions\BadRequestException;

class CanvasService
{

    private $cache;

    public static function getInstance(CacheInterface $cache)
    {
        return new static($cache);
    }

    public function __construct(CacheInterface $cache)
    {
        $this->cache = $cache;
    }

    public function create(Canvas $canvas): Canvas
    {
        return $this->cache->set($canvas);
    }

    public function move($movement_direction): Canvas
    {
        $canvas = $this->cache->get();
        $x = $canvas->getSpaceship()->getX();
        $y = $canvas->getSpaceship()->getY();
        switch (strtoupper($movement_direction)) {
            case 'TOP': {
                    --$y;
                    if ($y < 0) {
                        $y = ($canvas->getHeight() - 1);
                    }
                    break;
                }
            case 'RIGHT': {
                    ++$x;
                    if ($x > ($canvas->getWidth() - 1)) {
                        $x = 0;
                    }
                    break;
                }
            case 'BOTTOM': {
                    ++$y;
                    if ($y > ($canvas->getHeight() - 1)) {
                        $y = 0;
                    }
                    break;
                }
            case 'LEFT': {
                    --$x;
                    if ($x < 0) {
                        $x = ($canvas->getWidth() - 1);
                    }
                    break;
                }
            default: {
                    throw new BadRequestException('Invalid movement direction "' . $movement_direction . '".');
                }
        }
        $canvas->getSpaceship()->setX($x);
        $canvas->getSpaceship()->setY($y);
        return $this->cache->set($canvas);
    }

}
